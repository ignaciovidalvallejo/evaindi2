//funcion que carga la tienda de manera sincrona 
function carga(){
    $.ajax({
        async: false,
        url:"http://hispabyte.net/DWEC/entregable2-2.php"
    })
    .done(function(response){
        
        vm.productos=JSON.parse(response);
        //ordenacion por categoria
        vm.productos.sort(function(a,b){
            if(a.cat < b.cat){
                return -1;
            }else if(a.cat > b.cat){
                return 1;
            }else{
                return 0;
            }
        });
        
        console.log(response);
    })
}

function inicio(){
    //si no existe tienda en localstorage
    if(!localStorage.getItem("tienda")){
        //la carga por ajax
        carga();
        //y la guarda en localstorage
        localStorage.setItem("tienda",JSON.stringify(vm.productos));
        //y si existe la obtiene
    }else{
        vm.productos=JSON.parse(localStorage.getItem("tienda"));
    }
}

$(document).ready(inicio);



var vm = new Vue({
    el:"#app",
    data:{
        cat:"",
        nombre:"",
        id:"",
        unidades:"",
        precio:"",
        cantidad:"0",
        productos:[],
        carritos:[]
    },
    methods:{
        //funcion que comprueba que sean solo numeros
        numerico(cad){
            var reg = /^[^0-9]{1,}$/;
            if(reg.test(cad)){
                return false;    
            }else{
                return true;
            }
        },
        //funcion que añade el producto al array carritos y a localStorage
        anyadir(producto){

            if(this.numerico(this.cantidad)){
                var carr = {"id": producto.id , "cat": producto.cat , "nombre": producto.nombre , "unidades": this.cantidad , "precio": producto.precio};
                this.carritos.push(carr);
                this.cantidad=0;
                localStorage.setItem("carrito",JSON.stringify(this.carritos));
            }else{
                alert("introduce solo numeros");
            }
            
        },
        //funcion que vacia el carrito visualmente y de localStorage
        vaciarCarrito(){
            this.carritos=[];
            localStorage.setItem("carrito","");
        },
        //funcion que recarga la tienda
        recargarTienda(){
            carga();
            this.vaciarCarrito();
        },
        //funcion para ordenar array, de momento no he conseguido que funcione
        ordenaCat(array){
            array.sort(function (a, b){
                return a.cat < b.cat ? -1 : a.cat > b.cat ? 1 : 0;
            })
        }
    }
    
})

